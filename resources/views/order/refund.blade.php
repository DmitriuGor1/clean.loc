@extends('layouts.app')

@section('content')
    <main role="main" class="inner cover mt-5">
        <div class="container">
            <div class="raw">
                <div class="col-md-12">
                    <h2 class="text-center text-danger"><strong>Your funds are returned within 5-10 days as required by law <br><br>
                        They will be credited to the card with which payment was made<br><br>
                        We send your email with details of refund<br><br>
                        Thank you for being with us</strong></h2>    
                </div>
            </div>
        </div>
    </main> 
@endsection
