@extends('layouts.app')

@section('content')

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif

@if($errors->any())
    <div class="alert alert-danger">
        {!! implode('', $errors->all('<div>:message</div>')) !!}
    </div>
@endif

<h2 class="text-center">Enter your contact details</h2>
<div class="container">
    <form method="POST" action="{{ route('order.storeBack') }}" enctype="multipart/form-data">
        @csrf
        <div class="form-row">
            <div class="form-group col-md-6">
                <div class="input-group mb-3">
                    <label for="inputName" class="input-group-text bg-warning">Name</label>
                    <input type="text" name="name" value=" {{ !empty(old('name')) ? old('name') : $client_info->name }} " class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" id="inputName">
                    @if ($errors->has('name'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('name') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group col-md-6">
                <div class="input-group mb-3">
                    <label for="inputSurname" class="input-group-text bg-warning">Surname</label>
                    <input type="text" name ="surname" value="{{ !empty(old('surname')) ? old('surname') : $client_info->surname }}" class="form-control {{ $errors->has('surname') ? 'is-invalid' : '' }}" id="inputSurname">
                    @if ($errors->has('surname'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('surname') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <div class="input-group mb-3">
                    <label for="inputPhone" class="input-group-text bg-warning">Phone</label>
                    <input type="text" name ="phone" value="{{ !empty(old('phone')) ? old('phone') : $client_info->phone }}" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" id="inputPhone">
                    @if ($errors->has('phone'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group col-md-6">
                 <div class="input-group mb-3">
                    <label for="inputEmail" class="input-group-text bg-warning">Email</label>
                    <input type="email"  name="email" value="{{ !empty(old('email')) ? old('email') : $client_info->email }}" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" id="inputEmail">
                    @if ($errors->has('email'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <h2 class="text-center">Enter your address and flat area</h2>
        <div class="form-row ">
            <div class="form-group col-md-6">
                 <div class="input-group mb-3">
                    <label for="inputAddress" class="input-group-text bg-warning">Address</label>
                    <input type="text" name="address" value="{{ !empty(old('address')) ? old('address') : $client_order->address }}" class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" id="inputAddress" placeholder="">
                    @if ($errors->has('address'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('address') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group col-md-6">
                 <div class="input-group mb-3">
                    <label for="inputFlatArea" class="input-group-text bg-warning">Flat Area</label>
                    <input type="text" name="flat_area" value="{{ !empty(old('flat_area')) ? old('flat_area') : $client_order->flat_area }}" class="form-control {{ $errors->has('flat_area') ? 'is-invalid' : '' }}" id="inputFlatArea">
                    @if ($errors->has('flat_area'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('flat_area') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col text-center">
            @if(!empty($client_order->image))
            <img src="{{ asset('/storage/'. $client_order->image) }}" alt="flat">
            @else 
            <h2 class="text-center">If you need ... Upload photo</h2>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <div class="input-group mb-3">
                        <label for="inputImage" class="input-group-text bg-warning">Image</label>
                        <input type="file" name="image" value="{{ !empty(old('image')) ? old('image') : $client_order->image }}" class="form-control {{ $errors->has('image') ? 'is-invalid' : '' }}" class="form-control {{ $errors->has('image') ? 'is-invalid' : '' }} " id="inputImage" >
                        @if ($errors->has('image'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('image') }}</strong>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            @endif
        </div>
        <h2 class="text-center">Select your cleaning details</h2>
        <div class="form-row">
            <div class="form-group col-md-3">
                <div class="input-group mb-3">
                    <label for="inputRooms" class="input-group-text bg-warning">Rooms</label>
                    <select id="inputRooms" name="rooms" class="form-control {{ $errors->has('rooms') ? 'is-invalid' : '' }}">
                        @foreach (range(0, 10) as $roomsCount)
                            <option value="{{ $roomsCount }}"
                            @if(old('rooms') == $roomsCount || (empty(old('rooms')) && $client_order->rooms == $roomsCount)) selected @endif> 
                                {{ $roomsCount }}
                            </option>
                        @endforeach
                    </select>
                    @if ($errors->has('rooms'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('rooms') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group col-md-3">
                <div class="input-group mb-3">
                    <label for="inputBathroom" class="input-group-text bg-warning">Bathroom</label>
                    <select id="inputBathroom" name="bathroom" class="form-control {{ $errors->has('bathroom') ? 'is-invalid' : '' }}">
                        @foreach (range(0, 5, 0.5) as $bathroomCount)
                            <option value="{{ $bathroomCount }}" 
                            @if(old('bathroom') == $bathroomCount || (empty(old('bathroom')) && $client_order->bathroom == $bathroomCount)) 
                            selected @endif> 
                                {{ $bathroomCount }}
                            </option>
                        @endforeach
                    </select>
                     @if ($errors->has('bathroom'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('bathroom') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group col-md-3">
                <div class="input-group mb-3">
                    <label for="inputKitchen" class="input-group-text bg-warning">Kitchen</label>
                    <select id="inputKitchen" name="kitchen" class="form-control {{ $errors->has('kitchen') ? 'is-invalid' : '' }}">
                            @foreach (range(0, 10) as $kitchensCount)
                                <option value="{{ $kitchensCount }}" 
                                @if(old('kitchen') == $kitchensCount || (empty(old('kitchen')) && $client_order->kitchen == $kitchensCount)) 
                                selected @endif> 
                                {{ $kitchensCount }}
                                </option>
                            @endforeach
                    </select>
                    @if ($errors->has('kitchen'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('kitchen') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group col-md-3">
                <div class="input-group mb-3">
                    <label for="inputRefrigerator" class="input-group-text bg-warning">Refrigerator</label>
                    <select id="inputRefrigerator" name="refrigerator" class="form-control">
                        @foreach (range(0, 10) as $refrigeratorCount)
                            <option value="{{ $refrigeratorCount }}"
                             @if(old('refrigerator') == $refrigeratorCount || (empty(old('refrigerator')) && $client_order->refrigerator == $refrigeratorCount)) 
                                selected @endif> 
                                {{ $refrigeratorCount }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-3">
               <div class="input-group mb-3">
                    <label for="inputWardrobes" class="input-group-text bg-warning">Wardrobes</label>
                    <select id="inputWardrobes" name="wardrobes" class="form-control">
                        @foreach (range(0, 10) as $wardrobesCount)
                            <option value="{{ $wardrobesCount }}"
                              @if(old('wardrobes') == $wardrobesCount || (empty(old('wardrobes')) && $client_order->wardrobes == $wardrobesCount)) 
                                selected @endif> 
                                {{ $wardrobesCount }}
                            </option>
                        @endforeach
                    </select>
                    @if ($errors->has('wardrobes'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('wardrobes') }}</strong>
                        </div>
                    @endif
                </div>
            </div>

              <div class="form-group col-md-3">
                <div class="input-group mb-3">
                    <label for="inputAnimals" class="input-group-text bg-warning">Animals</label>
                    <select id="inputAnimals" name="animals" class="form-control">
                        @foreach (range(0, 10) as $animalsCount)
                            <option value="{{ $animalsCount }}"
                             @if(old('animals') == $animalsCount || (empty(old('animals')) && $client_order->animals == $animalsCount)) 
                                selected @endif> 
                                {{ $animalsCount }}
                            </option>
                        @endforeach
                    </select>
                    @if ($errors->has('animals'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('animals') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group col-md-3">
                <div class="input-group mb-3">
                    <label for="inputChildren" class="input-group-text bg-warning">Children</label>
                    <select id="inputChildren" name="children" class="form-control">
                        @foreach (range(0, 10) as $childrenCount)
                            <option value="{{ $childrenCount }}" 
                                @if(old('children') == $childrenCount || (empty(old('children')) && $client_order->children == $childrenCount)) 
                                selected @endif> 
                                {{ $childrenCount }}
                            </option>
                        @endforeach
                    </select>
                    @if ($errors->has('children'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('children') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group col-md-3">
                <div class="input-group mb-3">
                    <label for="inputAdults" class="input-group-text bg-warning">Adults</label>
                    <select id="inputAdults" name="adults" class="form-control">
                        @foreach (range(0, 10) as $adultsCount)
                            <option value="{{ $adultsCount }}" 
                            @if(old('adults') == $adultsCount || (empty(old('adults')) && $client_order->adults == $adultsCount)) 
                            selected @endif> 
                                {{ $adultsCount }}
                            </option>
                        @endforeach
                    </select>
                     @if ($errors->has('adults'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('adults') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        
            <div class="form-group col-md-12">
                <label for="">&nbsp;</label>
                <input type="submit"  value="Calculate Order" 
                class="btn btn-danger btn-block btn-flat">
            </div>
    </form>
</div>
@endsection