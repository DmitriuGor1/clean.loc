@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<script src="https://js.stripe.com/v3/"></script>
<form action="{{ route('order.pay') }}" method="post" id="payment-form">
    @csrf
    <div class="">
        <label for="card-element" class="text-center">
             Credit or debit card
        </label>
        <div id="card-element">
        <!-- a Stripe Element will be inserted here. -->
        </div>

    <!-- Used to display form errors -->
        <div id="card-errors" role="alert">
            @if(session()->has('message'))
                <div class="alert alert-danger">
                    {{ session()->get('message') }}
                </div>
            @endif
        </div>
    </div>

    <button>Submit Payment</button>
</form>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="{{ asset('js/charge.js') }}"></script>
@endsection






