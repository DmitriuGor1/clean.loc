@extends('layouts.app')

@section('content')
    <main role="main" class="inner cover mt-5">

    <h2 class="text-center">Order details:</h2>
        <div class="container">
            <div class="row">
                <div class="col">
                    <table class="table table-bordered">
                        <thead class="table-primary">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Order Amount</th>
                                <th scope="col">Address</th>
                                <th scope="col">Flat_area</th>
                                <th scope="col">Rooms</th>
                                <th scope="col">Bathroom</th>
                                <th scope="col">Kitchen</th>
                                <th scope="col">Refrigerator</th>
                                <th scope="col">Wardrobes</th>
                                <th scope="col">Animals</th>
                                <th scope="col">Children</th>
                                <th scope="col">Adults</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($order))
                            <tr>
                                <th scope="row">1</th>
                                <td>{{ $order->order_ammount }} USD</td>
                                <td>{{ $order->address }}</td>
                                <td>{{ $order->flat_area }}</td>
                                <td>{{ $order->rooms }}</td>
                                <td>{{ $order->bathroom }}</td>
                                <td>{{ $order->kitchen }}</td>
                                <td>{{ $order->refrigerator }}</td>
                                <td>{{ $order->wardrobes }}</td>
                                <td>{{ $order->animals }}</td>
                                <td>{{ $order->children }}</td>
                                <td>{{ $order->adults }}</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    @if(!empty($order->image))
                    <img src="{{ asset('/storage/'. $order->image) }}" alt="flat">
                    @else 
                    <h4>Without image</h4>
                    @endif
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                     <a class="btn btn-warning" href="{{ route('back') }}">Back</a>
                </div>
                <div class="col-md-8">
                </div>
                <div class="col-md-2">
                    <div class="button-pay">
                        <a class="btn btn-primary" href="{{ route('order.payments') }}">Go to Pay</a>
                    </div>
                </div>
            </div>  
        </div>
    </main> 
@endsection
