<h1>Order details</h1>
<img src="{{ env("APP_URL"). '/storage'.$order->image }}">
<p>Order Amount: <span>{{$order->order_ammount}}</span></p>
<p>Address: <span>{{$order->address}}</span></p>
<p>Flat_area: <span>{{ $order->flat_area }}</span></p>
<p>Rooms: <span>{{$order->rooms}}</span></p>
<p>Bathroom: <span>{{$order->bathroom}}</span></p>
<p>Kitchen: <span>{{$order->kitchen}}</span></p>
<p>Refrigerator: <span>{{$order->refrigerator}}</span></p>
<p>Wardrobes: <span>{{$order->wardrobes}}</span></p>
<p>Animals: <span>{{$order->animals}}</span></p>
<p>Children: <span>{{$order->children}}</span></p>
<p>Adults: <span>{{$order->adults}}</span></p>
