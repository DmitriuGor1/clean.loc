@extends('layouts.app')

@section('content')
<main role="main" class="inner cover mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h4 class="text-success text-centr">
                    <strong>Thank You for your Order</strong>
                </h4>
            </div>
        </div>
    </div>  
     <h2 class="text-center">Your order:</h2>
        <div class="container">
            <div class="row">
                <div class="col">
                    <table class="table table-bordered">
                        <thead class="table-primary">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Order Amount</th>
                                <th scope="col">Address</th>
                                <th scope="col">Flat_area</th>
                                <th scope="col">Rooms</th>
                                <th scope="col">Bathroom</th>
                                <th scope="col">Kitchen</th>
                                <th scope="col">Refrigerator</th>
                                <th scope="col">Wardrobes</th>
                                <th scope="col">Animals</th>
                                <th scope="col">Children</th>
                                <th scope="col">Adults</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>{{ $order->order_ammount }} USD</td>
                                <td>{{ $order->address }}</td>
                                <td>{{ $order->flat_area }}</td>
                                <td>{{ $order->rooms }}</td>
                                <td>{{ $order->bathroom }}</td>
                                <td>{{ $order->kitchen }}</td>
                                <td>{{ $order->refrigerator }}</td>
                                <td>{{ $order->wardrobes }}</td>
                                <td>{{ $order->animals }}</td>
                                <td>{{ $order->children }}</td>
                                <td>{{ $order->adults }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
            </div>
        </div>
    </div>  

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h4>Cancel the order for one click</h4>
            </div>
            <div class="col-md-6">
                <a class="btn btn-danger" href="{{ route('order.refundView') }}">Click</a>
            </div>
        </div>
    </div>  
</main>
@endsection
