<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- datatables -->

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    
    
</head>
<body>
    <div class="wrapper">
        <div class="content">
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="{{ route('order') }}">Cleaning company</a>
  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
        <li class="nav-link">
          @if((Gate::allows('super_admin')) || (Gate::allows('admin')))
                <a class="nav-link" id="" href="{{ route('admin') }}"><strong>Admin</strong></a>
            @endif
      </li>
      <li class="nav-link">
            @guest
                <a class=" {{ (request()->is('login')) ? 'active' : '' }}" href="{{ route('login') }}"><strong>{{ __('Login') }}</strong></a>
                @if (Route::has('register'))
                    <a class="nav-link {{ (request()->is('register')) ? 'active' : '' }}" href="{{ route('register') }}"><strong>{{ __('Register') }}</strong></a>
                @endif
                @else 
                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><strong>
                {{ __('Logout') }}<strong></a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                    </form>
            @endguest
      </li>
    </ul>
  </div>
</nav>
<main class="py-4">
    @yield('content')
</main>
        <!-- Footer -->
<!-- Footer -->
</div>
<footer class="page-footer font-smal l tealpt-4 bg-primary" id="footer">

<!-- Footer Text -->
    <div class="container-fluid text-center text-md-left">

    <!-- Grid row -->
        <div class="row">

    <!-- Grid column -->
            <div class="col-md-6 mt-md-0 mt-3">

        <!-- Content -->
                <h5 class="text-uppercase font-weight-bold text-center">Footer text 1</h5>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Expedita sapiente sint, nulla, nihil
                repudiandae commodi voluptatibus corrupti animi sequi aliquid magnam debitis, maxime quam recusandae
                harum esse fugiat. Itaque, culpa?</p>

            </div>
        <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none pb-3">

            <!-- Grid column -->
            <div class="col-md-6 mb-md-0 mb-3">

        <!-- Content -->
                <h5 class="text-uppercase font-weight-bold text-center">Footer text 2</h5>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Optio deserunt fuga perferendis modi earum
                    commodi aperiam temporibus quod nulla nesciunt aliquid debitis ullam omnis quos ipsam, aspernatur id
                    excepturi hic.</p>

            </div>
        <!-- Grid column -->

        </div>
    <!-- Grid row -->

    </div>
    <!-- Footer Text -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2020 Copyright:
    </div>
  <!-- Copyright -->

</footer>
<!-- <script src="https://cdn.datatables.net/plug-ins/1.10.15/api/row().show().js"></script> -->
</div>
</body>
</html>
