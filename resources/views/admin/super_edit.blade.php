@extends('layouts.app')

@section('content')

 @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
@endif
    <div class="card">
        <div class="card-header bg-warning">
            <h3 class="card-title text-center">Edit Admin</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form action="{{ route('storeUser', ['user_id' => $user->id]) }}" method="POST">
                {{ csrf_field() }}
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text bg-warning">
                            Name
                        </div>
                    </div>
                    <input type="text" name="name"
                           class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                           value="{{!empty(old('name')) ? old('name') : $user->name}}"
                           placeholder="">
                    @if ($errors->has('name'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('name') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text bg-warning">
                            Email
                        </div>
                    </div>
                    <input type="email" name="email"
                           class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                           value="{{!empty(old('email')) ? old('email') : $user->email}}"
                           placeholder="">
                    @if ($errors->has('email'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text bg-warning">
                            Password
                        </div>
                    </div>
                    <input type="password" name="password"
                           class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                           value=""
                           placeholder="">
                    @if ($errors->has('password'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text bg-warning">
                            Confirm 
                        </div>
                    </div>
                    <input type="password" name="password_confirmation"
                           class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}"
                           value=""
                           placeholder="">
                    @if ($errors->has('password_confirmation'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </div>
                    @endif
                </div>
                <input type="submit"  value="Edit" 
                class="btn btn-danger btn-block btn-flat">
            </form>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
@endsection
