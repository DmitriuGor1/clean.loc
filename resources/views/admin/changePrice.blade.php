@extends('layouts.app')

@section('content')

@if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
@endif
                
<main role="main" class="inner cover mt-5">
    <h1 class="text-center">Current Price</h1>
        <div class="container">
            <table class="table">
                <thead class="table-primary">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Price</th>
                        <th scope="col">Edit Price</th>
                        <th scope="col">Edit</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($prices as $price)
                            <tr>
                                <th scope="row">{{ $price->id }}</th>
                                <td>{{ $price->title }}</td>
                                <td>{{ $price->price }}</td>
                                <td>
                                    <form action="{{ route('admin.editPrice')}}" method="POST">
                                    @csrf
                                        <input type="text" name="price" value="{{ $price->price }}" class="col-xs-2 "><br>
                                        <input type="hidden" name="title" value="{{ $price->title }}"><br>
                                    </form>        
                                </td>
                                <td><button type="submit" onclick="return myFunction();" class="btn btn-primary">Edit</button></td>
                            </tr>
                    @endforeach
                </tbody>
            </table>  
        </div>                        
</main>
<script>
  function myFunction() {
      if(!confirm("Are You Sure to edit Price"))
      event.preventDefault();
  }
 </script> 
@endsection
