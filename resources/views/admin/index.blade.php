@extends('layouts.app')

@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    
<main role="main" class="inner cover mt-5">
    <h2 class="text-center">Admin Page</h2>
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <h4><a href="{{ route ('admin.refund', ['id' => $id])}}" class="badge badge-primary">Refunds</a></h4>
            </div>
            <div class="col-md-2">
                <h4><a href="{{ route ('admin.change_price', ['id' => $id])}}" class="badge badge-primary">Edit Price</a></h4>
            </div>
            <div class="col-md-2">
                <h4><a href="{{ route ('admin.change_pass', ['id' => $id])}}" class="badge badge-primary">Change Pass</a></h4>
            </div>
            @if((Gate::allows('super_admin')))
                <div class="col-md-2">
                    <h4><a href="{{ route ('admin.super', ['id' => $id])}}" class="badge badge-primary">Super</a></h4>
                </div>
            @endif
                <div class="col-md-2">
                    <h4><a href="{{ route ('admin.paymentsErrors', ['id' => $id])}}" class="badge badge-primary">Payment Errors</a></h4>
                </div>
        </div>
    </div>
        <div class="container">
            <table class="table ">
                <thead class="table-primary">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Order total amount per month</th>
                        <th scope="col">Order total canceled amount per month</th>
                        <th scope="col">Order quantity per month</th>
                        <th scope="col">Order avarage cost</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th scope="row">Items</th>
                    <td>{{ $data_payment['total_price'] }} USD</td>
                    <td>{{ $canceled_amount }} USD</td>
                    <td>{{ $data_payment['total_quantity'] }}</td>
                    <td>{{ $data_payment['order_avarage'] }} USD</td>
                    </tr>
                </tbody>
            </table>

            <table class="table">
                <thead class="table-primary">
                    <tr>
                        <th scope="col">ClientId</th>
                        <th scope="col">Name</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Sq.m</th>
                        <th scope="col">Status</th>
                        <th scope="col">Order details</th>
                    </tr>
                </thead>
                <tbody>
                @if (count($orders) > 0)
                @foreach ($orders as $order) 
                        <tr>
                            <th scope="row">{{ $order->client->id }}</th>
                            <td>{{ $order->client->name }}</td>
                            <td>{{ $order->client->phone }}</td>
                            <td> {{ $order->order_ammount }}</td>
                            <td> {{ $order->flat_area }}</td>
                            <td>
                                <form action="{{ route('admin.changeStatus', ['id' => $order['id']]) }}" method="POST">
                                @csrf
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ $order->status }}
                                        </button>
                                        <div class="dropdown-menu"> 
                                           <input type="submit" name="status" value="completed" class="dropdown-item">
                                           <input type="submit" name="status" value="canceled" class="dropdown-item">
                                           <input type="submit" name="status" value="paid" class="dropdown-item">
                                        </div>
                                    </div>
                                </form>
                            </td>
                            <td><a href="{{ route('admin.orderDetails', ['id' => $order->id]) }}">Order details</a></td>
                        </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
</main>
@endsection
