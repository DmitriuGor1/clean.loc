@extends('layouts.app')

@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    
<main role="main" class="inner cover mt-5">
    <!-- <h2 class="text-center">Refunds</h2> -->
        <div class="container">
            <table class="table">
                <thead class="table-danger">
                    <tr>
                        <th scope="col">Payment_Id</th>
                        <th scope="col">Order_Id</th>
                        <th scope="col">Client_Id</th>
                        <th scope="col">ClientName</th>
                        <th scope="col">Address</th>
                        <th scope="col">Status</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Refunds</th>
                    </tr>
                    @if(count($payments) > 0)
                    @foreach ($payments as $payment) 
                    <tr>
                        <th scope="col">{{ $payment->id }}</th>
                        <th scope="col">{{ $payment->order->id }}</th>
                        <th scope="col">{{ $payment->client_id }}</th>
                        <th scope="col">{{ $payment->order->client->name }}</th>
                        <th scope="col">{{ $payment->order->address }}</th>
                        <th scope="col">{{ $payment->order->status }}</th>
                        <th scope="col">{{ $payment->order->order_ammount }}</th>
                        <th>
                            
                            <form action="{{ route('admin.makeRefund', ['order_id' => $payment->order_id, 'id' => $payment->id]) }}" method="POST">
                            @csrf
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-dark">
                                        Refund
                                    </button>
                                </div>
                            </form>
                        </th>
                    </tr>
                    @endforeach
                    @else
                    <h3 class="text-danger">NoOne refund for this time</h3>
                    @endif
                </thead>
            </table>
        </div>
</main>
@endsection
