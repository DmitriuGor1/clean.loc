<h1>Refund details</h1>

<p>Refund Amount: <span>{{ $payment->order->order_ammount}}</span></p>
<p>Address: <span>{{ $payment->order->address }}</span></p>
<p>Flat_area: <span>{{ $payment->order->flat_area }}</span></p>
<p>Rooms: <span>{{ $payment->order->rooms }}</span></p>
<p>Bathroom: <span>{{ $payment->order->bathroom }}</span></p>
<p>Kitchen: <span>{{ $payment->order->kitchen }}</span></p>
<p>Refrigerator: <span>{{ $payment->order->refrigerator }}</span></p>
<p>Wardrobes: <span>{{ $payment->order->wardrobes }}</span></p>
<p>Animals: <span>{{ $payment->order->animals }}</span></p>
<p>Children: <span>{{ $payment->order->children }}</span></p>
<p>Adults: <span>{{ $payment->order->adults }}</span></p>