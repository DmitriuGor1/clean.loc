@extends('layouts.app')

@section('content')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
    
<!-- <main role="main" class="inner cover mt-5"> -->
    <!-- <h2 class="text-center">Payment Errors</h2> -->

        <!-- <div class="container"> -->
           <!--  <div class="custom-control custom-checkbox col-md-6">
              
                <a href="{{ route('admin.simple') }}">Simple DataTable</a>
                <a href="{{ route('admin.ajax') }}">Ajax DataTable</a>
                <p>show error payments with trashe</p><input type="checkbox" id="checkbox">
                
            </div> -->
        <!-- </div> -->

        <div class="container">
            <h4 class="text-center"><strong>Current payment Errors</strong></h4>
            <div class="custom-checkbox">
                <p class="text-left"><input type="checkbox" class="custom-control-input" 
                id="show_errors_checkbox">
                <label class="custom-control-label" 
                for="show_errors_checkbox">
                Payments with trashed
                </label></p>
            </div>
            <table class="table " id="payments_datatable">
                <thead class="table-primary">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">ClientId</th>
                        <th scope="col">OrderId</th>
                        <th scope="col">Status</th>
                        <th scope="col">Amount</th>
                        <th class="big-col">Stripe Response</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

<script>

    var payments_backend_url = '{{ route('admin.getForAjaxWithTrashed') }}';
    var payments_datatable;
    $(document).ready(function () {
        payments_datatable = $('#payments_datatable').DataTable({
            'processing' : true,
            'serverSide' : true,
            ajax : payments_backend_url,
            'columns' : [
                {'data' : 'id'},
                {'data' : 'client_id'},
                {'data' : 'order_id'},
                {'data' : 'status'},
                {'data' : 'order_ammount'},
                {'data' : 'stripe_response'}
            ]
        });
        
        $('#show_errors_checkbox').on('change', function () {
            if (this.checked) {
                payments_datatable.ajax.url(payments_backend_url + '?errors=1');
                payments_datatable.draw();
            } else {
                payments_datatable.ajax.url(payments_backend_url + '?errors=0');
                payments_datatable.draw();
            }
        });
    });
</script>
@endsection