@extends('layouts.app')

@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    
<main role="main" class="inner cover mt-5">
    <h2 class="text-center">Super Admin Page</h2>
        <div class="container-fluid">
                <table class="table ">
                    <thead class="table-primary">
                        <tr>
                            <th scope="col">Item</th>
                            <th scope="col">Id</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Password</th>
                            <th scope="col">Create</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <th scope="row">#</th>
                            <td>{{ $user->id }} </td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }} </td>
                            <td>{{ $user->password }} </td>
                            <td><a class="btn btn-success" href="{{ route('createUser') }}">Create</a></td>
                            <td><a class="btn btn-info admin" href="{{ route('editUser', ['user_id' => $user->id]) }}">Edit</a></td>
                            <td><a class="btn btn-danger" onclick="return myFunction();" href="{{ route('deleteUser', ['user_id' => $user->id]) }}">Delete</a></td>
                        </tr>
                         @endforeach
                    </tbody>
                </table>
    </div>
</main>
<script>
  function myFunction() {
      if(!confirm("Are You Sure to delete admin"))
      event.preventDefault();
  }
 </script>
@endsection
