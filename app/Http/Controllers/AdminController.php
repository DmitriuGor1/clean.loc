<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Order;
use App\Payment;
use App\Client;
use App\Price;
use App\User;
use App\Http\Requests\StorePriceRequest;
use App\Http\Requests\UserPassStoreRequest;
use App\Http\Requests\EditUserRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Libs\AdminLib;
use Mpdf\Mpdf;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;


class AdminController extends Controller
{
    public function index(Request $request)
    {
        $orders = (new AdminLib)->getOrdersForMonth();
        $canceled = (new AdminLib)->getCanceledOrders();
        $canceled_amount = (new AdminLib)->getCanceledAmount($canceled);
        $canceled_quantity = (new AdminLib)->getCanceledQuantity($canceled);
        $data_payment = (new AdminLib)->getAverageOrder($canceled_amount, $canceled_quantity);
        
        return (
            view(
                'admin.index',
                [
                    'orders' => $orders,
                    'data_payment' => $data_payment,
                    'id' => Auth::id(),
                    'canceled_amount' => $canceled_amount,
                ]
            )
        );
    }

    public function changePrice()
    {
        $prices = (new AdminLib)->getPrice();

        return view('admin.changePrice', [ 'prices' => $prices]);
    }

    public function editPrice(StorePriceRequest $request)
    {
        if (Auth::user()->cant('editPrice', User::class)) {
            return redirect()->route("admin");
        }
        $price = (new AdminLib)->updatePrice($request->all());

        return redirect()->route(
            'admin.change_price',
            [
                'price' => $price
            ])->with(
                'message',
                "Price for " . $request->title . " was changed!"
        ); 
    }

    public function changePass($id)
    {
        $user = (new AdminLib)->getUser($id);
        if (empty($user)){
            return redirect()->route('admin')->withErrors([
                "Admin with #" . $id . " doesn't exists"
            ]);
        } else {
            return view('admin.change_pass', ['user' => $user]);
        }
    }

    public function storePass(UserPassStoreRequest $request)
    {
        $user = (new AdminLib)->storePass(Auth::id(), $request->password);
        //sendEmail for admin
        $mail = AdminLib::sendMaiL(Auth::id());

        return redirect()->route('admin')->with(
            'message',
            "User #" . Auth::id() . " password was changed!"
        );
    }

    public function superAdmin()
    {
        return view('admin.super_admin', ['users' => User::all()]);
    }

    public function changeStatus(Request $request)
    {
        $order = (new AdminLib)->storeStatus($request->all());

        return redirect()->route('admin', ['order' => $order])->with(
            'message',
            "Order #" . $request->id . " status was changed!"
        );    
    }

    public function orderDetails(Request $request)
    {
        $orders = (new AdminLib)->getOrderDetails($request->id);

        return view('admin.orderDetails', ['orders' => $orders]);
    }

    public function refund()
    {
        $payments = (new AdminLib)->getPaymentsRefundforAdmin();

        return view('admin.refund', ['payments' => $payments]);    
    }

    public function makeRefund(Request $request)
    {
        $payment = (new AdminLib)->makeAdminRefund($request->all());
     
        return redirect()->route('admin.refund')->with(
            'message',
            "Order #" . $payment->order_id . "  was refunded!"
        );  
    }

    public function deleteUser(Request $request)
    {
        return (new AdminLib)->superDeleteUser($request->user_id);
    }

    public function editUser(Request $request)
    {
        $user = (new AdminLib)->getUser($request->user_id);
        if (empty($user)) {
            return redirect()->route(
                'admin.super', 
                ['id' => Auth::id()])->withErrors([
                "User with #" . $request->user_id . " doesn't exists"
            ]);
        }
        return view('admin.super_edit', ["user" => $user]); 
    }

    public function storeUser(EditUserRequest $request)
    {
        return (new AdminLib)->storeUser($request->all());
    }

    public function createUser(Request $request)
    {
        return view('admin.super_create');
    }

    public function superAdminUser(EditUserRequest $request)
    {
        return (new AdminLib)->superAdminCreateUser($request->all());
    }

    public function refundView(Request $request)
    {   
        (new AdminLib)->refundViewPayment($request->session()->get('order_id'));

        return view('order.refund');
    }

    public function paymentsErrors(Request $request)
    {   
            //dump($request->all());
            $payments = (new AdminLib)->getErrorPaymentsWithTrashed();
             return view(
            'admin.payments_errors', 
            [
                'payments' => $payments,
            ]);
        
    }

    public function simpleDataTable()
    {   
        $payments = (new AdminLib)->getErrorPaymentsWithTrashed(); 
        return view(
            'admin.simple',
                 [
                    'payments' => $payments,
                 ]
            );
    }

    public function ajaxDataTable()
    {   
        return view('admin.ajax');
    }

    public function getForAjax(Request $request)
    {   
        $query = (new AdminLib)->getErrorPayments();

        return datatables($query)->make(true);  // result JSON
    }

    public function getForAjaxWithTrashed(Request $request) 
    {
        //dd($request['errors']);
        if ($request['errors'] == 0 || is_null($request['errors'])) {

            $query = (new AdminLib)->getErrorPayments();
            //dd($query);
            return datatables($query)->make(true);  // result JSON
            //dd($query);
            //return $query;

        } else {

            $query = (new AdminLib)->getErrorPaymentsWithTrashed();
            //dd($query);
            $query = datatables($query)->make(true);  // result JSON

            //dd($query);
            return $query;
        }
    }
}
