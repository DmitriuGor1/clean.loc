<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Order;
use App\Price;
use App\User;
use App\Libs\OrderLib;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use App\Payment;
use App\Http\Requests\OrderStoreRequest;
use Illuminate\Support\Facades\View;
use Mpdf\Mpdf;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Exception;
use App\Exceptions\OrderPhoneException;


class OrderController extends Controller
{
    public function index()
    {
        return view('order.index');
    }

    public function back(Request $request)
    {
        if($request->session()->has('order_id')) {

            return view('order.back', [
                'client_order' => OrderLib::getOrder(
                $request->session()->get('order_id')
            ), 
                'client_info' => OrderLib::getClient(
                    $request->session()->get('id'))
            ]); 
        } else {
            return redirect()->route('order'); 
        }
    }

    public function store(OrderStoreRequest $request)
    {   
        $client = OrderLib::createClient($request->all());
        $order = OrderLib::createOrder($request->all(), $client); 
        
        return view('order.store',  ['order' => $order, 'client' => $client]); 
    }

    public function storeBack(OrderStoreRequest $request)
    {
        try {
            $client = OrderLib::updateClient($request->all());
        } catch (OrderPhoneException $e) {
            return redirect()->route('back')->withInput()->withErrors(
                $e->getMessage()
            );
        }
        $order = OrderLib::updateOrderBack(
            $request->all(), $request->session()->get('order_id')
        );
        
        return view('order.store', ['order' => $order, 'client' => $client]); 
    }

    public function payments(Request $request)
    {
        return view('order.payments');
    }

    public function pay(Request $request)
    {
        $order = OrderLib::getOrder(session()->get('order_id'));
        $client = OrderLib::getClient(session()->get('id'));

        //stripe charge
        $charge = OrderLib::calculateCharge(
            $request->stripeToken, $order->order_ammount, $client
        );

        if($charge['status'] === 'ok') {
            $orderSave = OrderLib::paymentCreateOrderSave(
                $order, $client, $charge
            );
        } else {
            OrderLib::createErrorPayment($charge, $order, $client);
            return redirect()->back()->with(
                'message',
                $charge['error']
            );
        }
        //Send Mail with PDF to client and admin
        OrderLib::sendMailWithPdf($order);

        return view('order.pay', ['order' => $order]);
    }
}

