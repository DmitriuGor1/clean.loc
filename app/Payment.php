<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['client_id', 'status', 'order_ammount', 'address', 'order_id', 'stripe_response'];
    protected $with = ['order'];

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id', 'id');
    }
}
