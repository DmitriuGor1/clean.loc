<?php

namespace App\Libs;

use App\Order;
use App\Client;
use App\User;
use App\Price;
use App\Payment;
use Mpdf\Mpdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\StorePriceRequest;
use App\Http\Requests\UserPassStoreRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;


class AdminLib
{
    private $currentMonth = '';

    public  function __construct()
    {
        $this->currentMonth = date('m');
    }

    // methods for change pass
    public static function sendMail()
    {
        $user = (new AdminLib)->getUser(Auth::id());
        Mail::raw(
            "Password was changed",
            function($message) use ($user) {
                $message->from('clean@loc.com', 'Admin');
                $message->to($user->email);
            }
        );

        return true;
    }

    // methods for change price
    public function getPrice()
    {
        return Price::all();
    }

    //methods for Average Price
    public function getOrdersForMonth()
    {
        $orders = Order::whereRaw(
            'Month(created_at) = ?',
            [$this->currentMonth]
        )->get();

        return $orders;
    }

    public function getAverageOrder(
        int $canceled_amount = 0, 
        int $canceled_quantity = 0
    ) {
        $data = Payment::whereRaw(
            'Month(created_at) = ?',
            [$this->currentMonth]
        )->where('status', '<>', 'error')->get();
        $data_payment = [];
        $data_payment['total_price'] = $data->sum('order_ammount');
        $data_payment['total_quantity'] = $data->count('id');
        $data_payment['order_avarage'] = (
            empty($data_payment['total_quantity'])
            || $data_payment['total_quantity'] === $canceled_quantity 
                ? 0
                : ($data_payment['total_price'] - $canceled_amount ) 
                /
                ($data_payment['total_quantity'] - $canceled_quantity)
            );
        return $data_payment;
    }

    // methods to change Password and edit Price and order detailss

    public function updatePrice(array $data)
    {
        return Price::where(
            'title', $data['title']
        )->update([
            'price' => $data['price']
        ]);
    }

    public function getUser($id)
    {
        return User::find($id);
    }

    public function storePass($id, $pass) 
    {
        return User::where(
            "id", $id
        )->update(
            ["password" => Hash::make($pass)]
        );
    }

    public function storeStatus(array $data)
    {
        return Order::where(
            'id', $data['id']
        )->update(
            ['status' => $data['status']]
        );
    }

    public function getOrderDetails(int $id)
    {
        return Order::where('id', $id)->get();
    }

     // methods for Stripe Refund

    public function getPaymentsRefundforAdmin()
    {
        return Payment::where('cancellations', '1')->get();
    }

    public function makeAdminRefund(array $array)
    {
        // colection
        $payments = Payment::where('order_id', $array['order_id'])->get();
        if (!empty($payments)) {
            foreach ($payments as $payment) {
                $stripe_response = json_decode($payment->stripe_response, true);
            }
        }
        $refund = self::refundStripe($stripe_response);
        $payment_update = self::updatePayment($refund, $payment);
        $order_update = self::updateOrder($payment);
        $data = self::makeViewPdf($payment);
        $email = self::sendRefundEmail($data);

        return $payment;
    }

    public static function refundStripe($stripe_response)
    {
        //dd($stripe_response);
        $stripe = new Stripe();
        $stripe = Stripe::make(env('STRIPE_API_KEY'));
        $refund = $stripe->refunds()->create($stripe_response['data']['id']);
        $refund = json_encode($refund);

        return $refund;
    }

    public static function makeViewPdf($payment)
    {
        $html = View::make(
            'admin.refund_pdf',
            ['payment' => $payment]
        )->render();
        $mpdf = new Mpdf();
        $mpdf->WriteHTML($html);
        $pdf = $mpdf->Output('', \Mpdf\Output\Destination::STRING_RETURN);
        $email = $payment->order->client->email;

        return ['email' => $email, 'pdf' => $pdf];
    }

    public static function sendRefundEmail($data)
    {
        $email = $data['email'];
        $pdf = $data['pdf'];
        Mail::raw("Your refund details", function($message) use ($email, $pdf)
        {
            $message->from('clean@admin.com', 'clean.loc');
            $message->to($email);
            $message->attachData($pdf, 'refund.pdf', [
                'mime' => 'application/pdf',
            ]);
        });
        return true;
    }

    public static function updatePayment($refund, $payment)
    {
        return Payment::where(
            "id", $payment->id
        )->update([
            'stripe_response' => $refund,
            'status' => 'refund',
            'cancellations' => '0'
        ]);
    }

    public static function updateOrder($payment)
    {
        return Order::where(
            'id', $payment->order_id
        )->update(['status' => 'canceled']);
    }

    public function storeUser(array $data)
    {
        $user = $this->getUser($data['user_id']);
        if (!empty($data['user_id']) && $data['email'] === $user->email ) {
            User::where(
                'id', $data['user_id']
            )->update(
                ['name' => $data['name'], 
                'password' => Hash::make($data['password'])]);

            return redirect()->route(
                'admin.super', 
                ['id' => Auth::id()])->with(
                'message',
                "User #" . $data['user_id'] . "  was Edit!"
            ); 
        } else {
            User::where(
                'id', $data['user_id']
            )->update(
                ['name' => $data['name'], 
                'email' => $data['email'], 
                'password' => Hash::make($data['password'])
            ]);
            return redirect()->route(
                'admin.super', 
                ['id' => Auth::id()])->with(
                'message',
                "User #" . $data['user_id'] . "  was Edit!"
            ); 
        } 
    }

    public function superAdminCreateUser(array $data)
    {
        if (empty($data)) {
            return redirect()->route(
                'admin.super', 
                ['id' => Auth::id()]
            )->withErrors(["Credentials are invalid"]);
        } 
        User::create([ 
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        return redirect()->route('admin.super',
         ['id' => Auth::id()])->with(
            'message',
            "User was Created!"
        );
    }

    public function superDeleteUser($id)
    {
        User::where('id', $id)->delete();
        
        return redirect()->route(
            'admin.super', 
            ['id' => Auth::id()]
        )->with(
            'message',
            "User #" . $id . "  was Deleted!"
        );  
    }

    public function refundViewPayment(int $id) 
    {   
        return Payment::where(
            'order_id', $id
        )->update(
            ['cancellations' => '1']
        );
    }

    public function getCanceledOrders()
    {
        return Order::where('status', 'canceled')->get();
    }

    public function getCanceledAmount($data)
    {
        if (!empty($data)) {
            return $data->sum('order_ammount');
        }
    }

    public function getCanceledQuantity($data)
    {
        if (!empty($data)) {
            return $data->count('id');
        }
    }

    public function getErrorPayments()
    {
        return Payment::whereRaw(
            'Month(created_at) = ?',
            [$this->currentMonth]
        )->get();
    }

    public function getErrorPaymentsWithTrashed()
    {
        return (
            Payment::withTrashed()
                ->whereRaw(
                    'Month(created_at) = ?',
                    [$this->currentMonth]
                )->get()
        );
    }
}
