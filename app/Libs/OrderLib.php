<?php 

namespace App\Libs;

use App\Order;
use App\Client;
use App\User;
use App\Price;
use App\Payment;
use Mpdf\Mpdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\OrderStoreRequest;
use Illuminate\Support\Facades\Storage;
use Image;
use Exception;
use App\Exceptions\OrderPhoneException;

class OrderLib
{
    private $pricing;
    //private $stripe;

    // public function __construct()
    // {
    //     $this->stripe = Stripe::make(env('STRIPE_API_SECRET_KEY')); 
    // }

    // methods for Price, calculate Order_amount

    public function calculateTotalPrice(Order $order)
    {
        $total_price = (
            ($order->flat_area * $this->getServicePrice('flat_area')) +
            ($order->rooms * $this->getServicePrice('rooms')) + 
            ($order->bathroom * $this->getServicePrice('bathroom')) +
            ($order->kitchen * $this->getServicePrice('kitchen')) +
            ($order->wardrobe * $this->getServicePrice('wardrobe')) +
            ($order->animals * $this->getServicePrice('animals')) +
            ($order->children * $this->getServicePrice('children')) +
            ($order->adults * $this->getServicePrice('adults')) +
            ($order->refrigerator * $this->getServicePrice('refregirator'))
        );

        return $total_price;
    }

    public function getServicePrice(string $serviceName)
    {
        if (is_null($this->pricing)) {
            $this->fetchPrices();
        }

        return (
            !empty($this->pricing[$serviceName])
                ? $this->pricing[$serviceName]
                : 0
        );
    }

    public function fetchPrices()
    {
        $this->pricing = [];
        $pricesModels = Price::get();
        if (!empty($pricesModels)) {
            foreach ($pricesModels as $value) {
                $this->pricing[$value->title] = $value->price;
            }
        }
    }

    // methods for route back
    public static function getOrder(int $order_id) 
    {   
        return Order::find($order_id);
    }

    public static function getClient(int $id) 
    {   
        return Client::find($id);
    }


    // methods for create and update client 
    public static function createClient(array $data)
    {    
        $client = Client::updateOrCreate(
            ['phone' => $data['phone']],
            ['email' => $data['email'], 
            'surname' => $data['surname'], 
            'name' => $data['name'],    
        ]);
        session(['client_id' => $client->id]);

        return $client; 
    }

    public static function createOrder(array $data, Client $client)
    {        
        $order = Order::create($data);
        $order = self::saveImage($data, $order, $client);
        $order->client_id = $client->id;
        $order->status = 'created';
        $order->order_ammount = (new OrderLib)->calculateTotalPrice($order);
        $order->save();
        session(['order_id' => $order->id, 'id' => $client->id]);
        return $order;

    } 

    public static function saveImage(array $data, Order $order)
    {    
        if (!empty($data['image'])) {
            $image = $data['image']; //get image
            // create image name
            $imagename = time() . '.' . $image->extension();
            //determine way for save image, here i'm put image
            // storage_path is the way for folder storage
            $destinationPath = storage_path('app/public/image'); 
            // $destinationPath = storage_path(env('IMAGE_STORAGE_PATH'));
            $img = Image::make($image->path());
            // resize image 
            $img->resize(
                300,
                300,
                function ($constraint) {
                    $constraint->aspectRatio();
                }
            )->save($destinationPath . '/' . $imagename);
            // $order->image = env('IMAGE_DB_LINK_PATH') . $imagename;
            $order->image = '/image/' . $imagename; // own get way to file
            $order->save();
        }
        return $order; 
    }


    public static function updateClient(array $data)
    {
        $client = self::getClient(session()->get('client_id'));
        $isPhoneFree = self::checkPhoneIsFree($data['phone'], $client);

        if (!$isPhoneFree) {
            throw new OrderPhoneException(
                'Client with this phone exists, enter another phone'
            );
        }

        if (
            $data['phone'] !== $client->phone 
            || $data['name'] !== $client->name 
            || $data['surname'] !== $client->surname 
            || $data['email'] !== $client->email
        ) {
            $client = Client::where('id', session('client_id'))->update([
                'name' => $data['name'],
                'email' => $data['email'],
                'surname' => $data['surname'],
                'phone' => $data['phone'],
            ]);
            return $client;
        }

        if ($data['phone'] === $client->phone) {
            $client = Client::where('phone', $client->phone)->update([
                'name' => $data['name'],
                'email' => $data['email'],
                'surname' => $data['surname']
            ]);
            return $client;
        }
    }

    public static function checkPhoneIsFree(int $phone, Client $client)
    {
        return (
            is_null(
                Client::where('phone', $phone)
                    ->where('id', '<>', $client->id)
                    ->first()
            )
        );
    }

    public static function updateOrderBack(array $data, int $id)
    {        
        Order::where('id', $id)->update([
            'address' => $data['address'],
            'flat_area' => $data['flat_area'],
            'rooms' => $data['rooms'],
            'bathroom' => $data['bathroom'],
            'kitchen' => $data['kitchen'],
            'refrigerator' => $data['refrigerator'],
            'wardrobes' => $data['wardrobes'],
            'animals' => $data['animals'],
            'children' => $data['children'],
            'adults' => $data['adults'],
        ]);

        $order = self::updateOrderBackAmount(session()->get('order_id'));
        $order = self::saveImage($data, $order);


        return $order;
    } 

    public static function updateOrderBackAmount(int $id)
    {        
        $order =  self::getOrder(session()->get('order_id'));
        $order->order_ammount = (new OrderLib)->calculateTotalPrice($order);
        $order->save();
        return $order;
    } 

    public static function calculateCharge(
        $token, int $amount, Client $client
    ) {
        return self::makeStripe($token, $amount, $client); 
    }

    public static function makeStripe($token, int $amount, Client $client)
    {   
        $stripe = Stripe::make(env('STRIPE_API_KEY'));
        // dd($stripe);
        if (empty($client->stripe_customer_id)) {
            self::makeCustomer($client, $stripe);
        }

        if (!empty($token)) {
            return self::makeCharge($token, $amount, $stripe);
        }
    }

    public static function makeCustomer(Client $client, $stripe)
    {   
        $customer = $stripe->customers()->create(
            [
                'email' => $client->email,
                'name' => $client->name,
            ]
        );
            $client->stripe_customer_id = $customer['id'];
            $client->save(); 

        return true;
    }

    public static function makeCharge($token, int $amount, $stripe)
    {   
        try {
            $charge =  $stripe->charges()->create(
                [
                    'amount' => $amount,
                    'currency' => 'usd',
                    'source' => $token,
                ]
            );
        } catch (Exception $e) {
            $message = $e->getMessage();
            return ['status' => 'error', 'error' => $message];
        }
        return['status'=> 'ok', 'data' => $charge];
    }

    public static function paymentCreateOrderSave(
        Order $order, Client $client, $charge
    ) {   
        $paymentData = [];
        $paymentData['client_id'] = $client->id;
        $paymentData['order_id'] = $order->id;
        $paymentData['order_ammount'] = $order->order_ammount; 
        $paymentData['status'] = 'fulfilled';
        $paymentData['stripe_response'] = json_encode($charge);
        $payment = Payment::create($paymentData); // save Payment into db
        $order->status = 'paid';
        $order->save();
        return $order;
    }

    public static function createErrorPayment(
        array $charge, Order $order, Client $client
    ) {   
        return Payment::create([
                    'stripe_response' => json_encode($charge),
                    'client_id' => $client->id,
                    'order_id' => '0',
                    'order_ammount' => $order->order_ammount,
                    'status' => 'error',
                ]);
    }

    public static function sendMailWithPdf(Order $order) 
    {   
        $users = self::getUsers(); // admin's email
        foreach ($users as $user) {
            $admin_emails = $user->email;
        }
        $html = View::make('order.export_pdf', ['order' => $order])->render();
        $mpdf = new Mpdf();
        $mpdf->WriteHTML($html);
        $pdf = $mpdf->Output('', \Mpdf\Output\Destination::STRING_RETURN);
        $emails = [$order->client->email, $admin_emails];

        Mail::raw(
            "Thank you for choose our company. 
            Here is Order details to your order # $order->id", 
            function($message) use ($emails, $pdf)
        {
            $message->from('clean@admin.com', 'clean.loc');
            $message->to($emails);
            $message->attachData($pdf, 'order.pdf', [
                'mime' => 'application/pdf',
            ]);
        });
        return true;
    }

    public static function getUsers()
    {   
        return User::all();
    }

    public static function getClients()
    {   
        return Client::all();
    } 
}