<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\User;
use App\Policies\UserPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        User::class => UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define(
            'super_admin',
            function ($user) {
                if ($user->is_super_admin == '1') {
                    return true;
                }
                return false;
            }
        );

        Gate::define(
            'admin',
            function ($user) {
                if ($user->is_super_admin == '0') {
                    return true;
                }
                return false;
            }
        );
    }
}
