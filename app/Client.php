<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Client extends Model
{
    protected $fillable = ['name', 'surname', 'email', 'phone', 'stripe_customer_id', 'add_info'];    
}
