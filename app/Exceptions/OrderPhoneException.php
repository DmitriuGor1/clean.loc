<?php

namespace App\Exceptions;

use Log;
use Exception;

class OrderPhoneException extends Exception
{
    public function report()
    {
        Log::debug(
            'Client with this phone exists, enter another phone'
        );
    }
}
