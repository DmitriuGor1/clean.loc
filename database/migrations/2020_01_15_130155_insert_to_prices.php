<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class InsertToPrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('prices')->insert([
            [
                'title' => 'bathroom',
                'price' => 4,
            ],
            [
                'title' => 'flat_area',
                'price' => 6,
            ],
            [
                'title' => 'kitchen',
                'price' => 6,
            ],
            [
                'title' => 'rooms',
                'price' => 2,
            ],
            [
                'title' => 'wardrobe',
                'price' => 3,
            ],
            [
                'title' => 'animals',
                'price' => 2,
            ],
            [
                'title' => 'children',
                'price' => 3,
            ],
            [
                'title' => 'adults',
                'price' => 2,
            ],
            [
                'title' => 'refregirator',
                'price' => 2,
            ],

        ]); 
    }

    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prices', function (Blueprint $table) {
            //
        });
    }
}
