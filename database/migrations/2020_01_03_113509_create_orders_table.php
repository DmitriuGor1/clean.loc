<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('client_id')->nullable();
            $table->string('status')->nullable();
            $table->decimal('order_ammount', 10, 2)->nullable();
            $table->string('address');
            $table->integer('flat_area');
            $table->integer('rooms')->nullable();
            $table->float('bathroom', 8, 1)->nullable();
            $table->float('kitchen', 8, 1)->nullable();
            $table->integer('refrigerator')->nullable();
            $table->integer('wardrobes')->nullable();
            $table->integer('animals')->nullable();
            $table->integer('children')->nullable();
            $table->integer('adults');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
