<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'OrderController@index')->name('order');
    Route::prefix('order')->group(function () {
        Route::post('store', 'OrderController@store')->name('order.store');
        Route::post('storeBack', 'OrderController@storeBack')->name('order.storeBack');
        Route::get('payments', 'OrderController@payments')->name('order.payments');
        Route::post('pay', 'OrderController@pay')->name('order.pay');
        Route::get('back', 'OrderController@back')->name('back');
        Route::get('refundView', 'AdminController@refundView')->name('order.refundView');
    });

Auth::routes(['register' => false]);

Route::middleware(['auth'])->group(function () {
    Route::prefix('admin')->group(function () {    
        Route::get('', 'AdminController@index')->name('admin');
        Route::get('changePrice', 'AdminController@changePrice')->name('admin.change_price');
        Route::get('changePass/{id}', 'AdminController@changePass')->name('admin.change_pass');
        Route::post('storePass', 'AdminController@storePass')->name('admin.store_pass');
        Route::post('storePrice', 'AdminController@storePrice')->name('admin.store_price');
        Route::post('changeStatus', 'AdminController@changeStatus')->name('admin.changeStatus');
        Route::post('editPrice', 'AdminController@editPrice')->name('admin.editPrice');
        Route::get('orderDetails/{id}', 'AdminController@orderDetails')->name('admin.orderDetails');
        Route::get('paymentsErrors', 'AdminController@paymentsErrors')->name('admin.paymentsErrors');
        Route::get('paymentsErrorsWithTrashed', 'AdminController@paymentsErrors')->name('admin.paymentsErrorsWithTrashed');
        Route::get('refund', 'AdminController@refund')->name('admin.refund');
        Route::post('makeRefund', 'AdminController@makeRefund')->name('admin.makeRefund');
        Route::get('superAdmin/{id}', 'AdminController@superAdmin')->name('admin.super');
        Route::get('deleteUser', 'AdminController@deleteUser')->name('deleteUser');
        Route::get('editUser', 'AdminController@editUser')->name('editUser');
        Route::post('storeUser', 'AdminController@storeUser')->name('storeUser');
        Route::get('createUser', 'AdminController@createUser')->name('createUser');
        Route::post('superAdminUser', 'AdminController@superAdminUser')->name('superAdminUser');

        Route::get('simple', 'AdminController@simpleDataTable')->name('admin.simple');
        Route::get('ajax', 'AdminController@ajaxDataTable')->name('admin.ajax');
        Route::get('getForAjax', 'AdminController@getForAjax')->name('admin.getForAjax');
        Route::get('getForAjaxWithTrashed', 'AdminController@getForAjaxWithTrashed')->name('admin.getForAjaxWithTrashed');
    });
});






